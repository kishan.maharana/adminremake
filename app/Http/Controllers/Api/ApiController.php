<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;
use App\User;
use App\Profile;
use DB;
use Auth;
use PDF;
use App;
use Illuminate\Support\Facades\Mail;
use App\Jobs\GeneratePdfMail;
class ApiController extends Controller
{

	public function allPost()
	{
        // getting all the record of posts table and user table where posts.user_id = users.id  API 1

		$data = DB::table('posts')
		->join('users', 'users.id', '=', 'posts.user_id')
		->select('users.*', 'posts.*')
		->get();
		return $data;
	}
	public function specificData($post_id)
	{
        //Getting a specific record using the post id API 2


		$data=DB::table('posts')
		->select('user_id')
		->where('id',$post_id)
		->get();
		$data=$data[0];
    	$data=$data->user_id; //Getting the user_id from posts table

    	$all = DB::table('posts')
    	->join('users', 'users.id', '=', 'posts.user_id')
    	->select('users.*', 'posts.*')
    	->where('users.id','=',$data)
    	->get();
        return response()->json($all); // getting a specific record from both users table and posts table
    }
    public function update($id)
    {
        // API for updating a specific record in posts table for specifi id API 4

    	Post::where("id",$id)
    	->update(array('posttitle' => 'updating this record'));
    	return 'updated';
    }
    public function delete($id)
    {
        // API for deleteting a specific record from posts table for a specific id API 3

    	DB::table('posts')
    	->join('users', 'users.id', '=', 'posts.user_id')
    	->where('posts.id','=',$id)
    	->delete();
    	return 'deleted';
    }
    public function userwithpost()
    {
        // API for getting all users data having post API 6

    	$data = DB::table('posts')
    	->join('users', 'users.id', '=', 'posts.user_id')
    	->select('users.*')
    	->get();
    	return response()->json($data);
    }
    public function databetweendates()
    {
        // API for fetching all the records from posts table between two perticular dates API 5

    	$alldata=DB::table('posts')
    	->whereBetween('created_at',['20-08-18','20-08-20'])
    	->get();
    	return response()->json($alldata);

    }
    public function postWithDates($id)
    {
        $data=DB::table('posts')
            ->join('users','users.id','=','posts.user_id')
            ->select('posts.posttitle AS Post_Name','posts.created_at AS Post_Date')
            ->where('posts.user_id','=',$id)
            ->whereBetween('posts.created_at',['20-08-18','20-08-20'])
            ->get();
            return response()->json($data);
    }
     public function sendPdf($id)
    {

        //API for sending mail with a pdf attachment with a specific data of an user

        $record=DB::table('posts')
            ->join('users','users.id','=','posts.user_id')
            ->select('users.*','posts.*')
            ->where('posts.user_id','=',$id)
            ->get();
       
        $final=array('record'=>$record);
       
            $user_detail=User::find($id);
            $email=$user_detail->email;
            $name=$user_detail->name;

            //Setting up Basic details for sending a Mail

            $data["email"]=$email;
            $data["client_name"]=$name;
            $data["subject"]='All Post';

            //Generating PDF with all the post details

            $pdf = PDF::loadView('PDFapi',$final);  

            //Sending Mail to the corresponding user

            Mail::send([], $data, function($message)use($data,$pdf) {
            $message->to($data["email"], $data["client_name"])
            ->subject($data["subject"])
            ->attachData($pdf->output(), 'Your_Post_Detail.pdf', [
                       'mime' => 'application/pdf',
                   ])
            ->setBody('Hi, welcome user! this is the body of the mail');
            });

                // GeneratePdfMail::dispatch($user_detail,$final)
                // ->delay(now()->addSeconds(10));
    }

}
