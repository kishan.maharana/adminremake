<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class chartController extends Controller
{
 public function __construct()
 {
        // here we have to provide the middleware class name not the middleware name
    $this->middleware('checkauth', ['only' => ['chartjs','float','inline']]); 

 }
public function chartjs()
{
    return view('admin.pages.charts.chartjs');
}
public function float()
{
    return view('admin.pages.charts.float');
}

public function inline()
{
    return view('admin.pages.charts.inline');
}
}
