<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;
class UserController extends Controller
{
    public function login()
	{
		// return view('Authentication.login');
		return redirect(url('/login'));
	}
	public function loginverify(Request $req)
	{
		$this->validate($req,[
			'email' => 'required|max:500',
			'password' => 'required',
		],[
			'email.required' => ' please provide your email',
			'password.required' => ' please provide your password',
		]);
		if(Auth::attempt(['email'=>$req->email,'password'=>$req->password]))
		{
			return redirect(url('/home'));
		}
		else
		{
			return redirect(url('/login'))->with('errmsg','unable to login');
		}

	}
	public function registerverify(Request $req)
	{
		$this->validate($req,[
			'name' => 'required|min:2|max:255',
			'email' => 'required|email|unique:users|max:255',
			'password' => 'required|confirmed|max:30',
		],[
			'name.required' => ' please provide your name',
			'name.min' => 'minimum 2 characters required',
			'email.required' => ' please provide your email',
			'email.unique' => ' This email is already exist',
			'password.required' => ' please provide your password',
			'password.confirmed' => 'password doesnot match with your confirm password',
		]);
		$data=new User;
		$data->name=$req->name;
		$data->email=$req->email;
		$data->password=bcrypt($req->password);
		$data->save();
		return redirect(url('/login'))->with('registered','Registration Successfull');

	}
	public function reset()
	{
		return view('auth.passwords.reset');
	}
}
