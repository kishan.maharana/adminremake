<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use PDF;
class GeneratePdfMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user_detail,$final)
    {
        $this->user_detail=$user_detail;
        $this->final=$final;
        
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
            $email=$user_detail->email;
            $name=$user_detail->name;

            //Setting up Basic details for sending a Mail

            $data["email"]=$email;
            $data["client_name"]=$name;
            $data["subject"]='All Post';

            //Generating PDF with all the post details

            $pdf = PDF::loadView('PDFapi',$final);  
            // $pdf=$pdf->output();


            // $pdf= $pdf->download('invoice.pdf');
            //Sending Mail to the corresponding user

            Mail::send([], $data, function($message)use($data,$pdf) {
            $message->to($data["email"], $data["client_name"])
            ->subject($data["subject"])
            ->attachData($pdf->output(), 'Your_Post_Detail.pdf', [
                       'mime' => 'application/pdf',
                   ])
            ->setBody('Hi, welcome user! this is the body of the mail');
            });
    }
}
