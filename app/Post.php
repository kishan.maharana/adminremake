<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    function myUser()
    {
        return $this->belongsTo('App\User'); 
    }
}
