<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    function myUser()
    {
        return $this->belongsTo('App\User'); 
    }
}
