<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class TestServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // sending variables to all the view files throughout the project

        view()->share('checking', [
            'providername' => 'Common variable 1',
            'providerproperty' => 'Common variable 2'
        ]);
        
    }
}
