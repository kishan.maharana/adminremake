@extends('layouts.app')


@section('content')

@if(session('registered'))

<p class="alert alert-success">{{session('registered')}}</p>

@endif
@if(session('errmsg'))

<p class="alert alert-danger">{{session('errmsg')}}</p>

@endif


<form action="{{url('/clogin')}}" method="post"> 
    {{csrf_field()}}
  <fieldset>
    <legend>Login</legend>
   
    <div class="form-group">
      <label for="exampleInputEmail1">Email address</label>
      <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
    
    </div>
    <div class="form-group">
      <label for="exampleInputPassword1">Password</label>
      <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
    </div>
    <div class="form-group">
      
      <input type="submit" class="form-control btn btn-primary" >
    
    </div>
    <div>
 <div class="form-group row mb-0">
                          

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                        </div>
    </div>
   
</form>



@endsection
