<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// dd(app()->make('hello'));
Route::get('/', function () {
    return view('welcome');
}); 

// using auth middleware to prevent unauthenticated user from accessing the routes

Route::group(['middleware' => ['checkauth']], function () {
Route::group(['prefix'=>'Dashboard'],function(){
 Route::get('/dashboard','AdminController@dashboard');
 Route::get('/dashboard2','AdminController@dashboard2');
 Route::get('/dashboard3','AdminController@dashboard3');
 Route::get('/pages/widget','AdminController@widget');
});
Route::group(['prefix'=>'/pages'],function(){
 Route::get('/topnav','LayoutController@topNav');
 Route::get('/topnavsidebar','LayoutController@topNavSidebar');
 Route::get('/boxed','LayoutController@boxed');
 Route::get('/fixedsidebar','LayoutController@fixedSidebar');
  Route::get('/fixednavbar','LayoutController@fixedNavbar');
  Route::get('/fixedfooter','LayoutController@fixedFooter');
  Route::get('/collapsedsidebar','LayoutController@collapsedSidebar');
  Route::get('/calender','LayoutController@calender');
  Route::get('/gallery','LayoutController@gallery');
});

Route::group(['prefix'=>'/charts'],function(){
 Route::get('/chartjs','ChartController@Chartjs');
  Route::get('/float','ChartController@float');
 Route::get('/inline','ChartController@inline');
});
Route::group(['prefix'=>'/ui'],function(){
 Route::get('/buttons','UiController@buttons');
 Route::get('/general','UiController@general');
 Route::get('/icons','UiController@icons');
 Route::get('/modals','UiController@modals');
 Route::get('/navbar','UiController@navbar');
 Route::get('/ribbons','UiController@ribbons');
 Route::get('/sliders','UiController@sliders');
 Route::get('/timeline','UiController@timeline');
});
Route::group(['prefix'=>'/forms'],function(){
 Route::get('/advanced','FormController@advanced');
Route::get('/editors','FormController@editors');
  Route::get('/general','FormController@general2');
  Route::get('/validation','FormController@validation');

});
Route::group(['prefix'=>'/tables'],function(){
 Route::get('/profiletable','TableController@data');
 Route::get('/posttable','TableController@jsgrid');
 Route::get('/alluser','TableController@simple');
});
Route::group(['prefix'=>'/mailbox'],function(){
 Route::get('/compose','MailboxController@compose');
 Route::get('/mailbox','MailboxController@mailBox');
 Route::get('/readmail','MailboxController@readMail');

});
});
Auth::routes();

// Routes that a unauthenticated user can access

Route::get('/home', 'HomeController@index')->middleware('api');
Route::get('/clogin','UserController@login')->name('clogin');
Route::get('/reset','UserController@reset')->name('resetp');
Route::post('/clogin','UserController@loginverify')->name('cloginverify');
Route::post('/cregister','UserController@registerverify')->name('cregisterverify');

//php unit testing routes

Route::get('/test',function(){
return 'true';
});
